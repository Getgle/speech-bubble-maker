var imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);
var canvas = document.getElementById('imageCanvas');
var ctx = canvas.getContext('2d');


function handleImage(e){
    canvas.height = null;
    canvas.width = null;
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();

        img.onload = function(){
            bubblespace = img.height*0.55;
            canvas.width = img.width;
            canvas.height = img.height + bubblespace;
            ctx.drawImage(img,0,bubblespace);
        }
        
        img.src = event.target.result;
        var bubble = new Image();
        bubble.src = "bubble.gif";
        bubble.onload = function(){
            ctx.drawImage(bubble, 0, 0, img.width, bubblespace);
          }
    }
    reader.readAsDataURL(e.target.files[0]);     
}